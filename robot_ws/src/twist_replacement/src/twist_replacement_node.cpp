#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <stdint.h>
#include <unistd.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>


#define O_RDONLY         00
#define O_WRONLY         01
#define O_RDWR           02


#include "ros/ros.h"
#include "std_msgs/String.h"

#include <sstream>

int fd;
FILE *fpm = NULL;

double Get_Time ()
{
    struct timespec now;
    clock_gettime (CLOCK_REALTIME, &now);
    return now.tv_sec + 1.e-9 * now.tv_nsec;
}

void
NotifyUser (int LINE, unsigned char *bufw, int n)
{
    if (n == -1)
    {
        printf ("\n ERROR in write <%d>  with %d in line %d\n", bufw[0], n, LINE);
    }
    else
    {
        printf ("\n %f SUCCES write <%d>  with %d in line %d\n", Get_Time (), bufw[0], n, LINE);
    }

    for (int i = 0; i < n; i++)
        printf ("(%d)%02hhX ", i, bufw[i]);
    printf ("\n");

    //fprintf (fichier, "%lf %d %d %d %d\n", Get_Time (), bufr[0], bufr[1], bufr[2], bufr[3]);
}


void open_port (char *argv)
{
    printf ("%s receives %s\n", __FUNCTION__, argv);

    struct termios new_port_settings;

    fd = open (argv, O_RDWR | O_NOCTTY | O_NONBLOCK | O_NDELAY);

    cfsetispeed (&new_port_settings, B115200);
    cfsetospeed (&new_port_settings, B115200);

    if (fd == -1)
    {
        printf ("error_open_port: Unable to open %s\n", argv);
        exit (EXIT_FAILURE);
    }
    else
    {
        printf ("arduino port is ready\n");
        printf ("O_RDWR is %d\n", O_RDWR);
        printf ("O_NOCTTY is %d\n", O_NOCTTY);
        printf ("O_NONBLOCK is %d\n", O_NONBLOCK);
        printf ("O_NDELAY is %d\n", O_NDELAY);
        fcntl (fd, F_SETFL, 0);
    }

    printf ("%s finished! go to the main !\n", __FUNCTION__);

}

void readOnce ()
{
    int R, L;
    char nomfichier[100], commande[1000];

    int n, count = 0, errcount = 0;
    unsigned int T, T_old = 0;
    char bufw[4] = { 0 }, bufr[4];

    // strcpy (bufw, "r\n\r");
    // n = write (fd, bufw, 4);

    strcpy (bufw, "e\n\r");    // preparing command to send
    n = write (fd, bufw, 4);   // sending

    n = read (fd, bufr, 4);

    L = bufr[2] + 256 * bufr[3];
    R = bufr[0] + 256 * bufr[1];

    //if (bufr[0] == 255 && bufr[1] == 255)
    {
        printf ("%lf, %d, %d\n", Get_Time(), R, L);
//        count++;
  //      T_old = T;
    }
    /*else
    //{
        char tmp[256];
        sprintf (tmp, "echo nb %d %lf 1_%d 2_%d 3_%d 4_%d %d %d >>  err",
                 errcount, Get_Time (), (unsigned char) bufr[0], (unsigned char) bufr[1], (unsigned char) bufr[2], (unsigned char) bufr[3], R, L);
        system (tmp);
        errcount++;
    }*/
	
    return;
}


int main(int argc, char **argv)
{

    ros::init(argc, argv, "talker");

    ros::NodeHandle n;

    ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);

    ros::Rate loop_rate(10);

// open port
    char port_to_open[32];

    printf ("argc=%d argv[0]=%s \n", argc, argv[0]);

    if (argc == 2)
    {
        printf ("argv[1]=%s\n", argv[1]);
        strcpy (port_to_open, argv[1]);
        printf ("\n port <%s> will be opened\n", port_to_open);
    }
    else
	{
        printf ("\n you need to specify the port of arduino:\n %s /dev/ttyACM0\n", argv[0]);
	    strcpy (port_to_open, "/dev/ttyACM2");
	}

    open_port (port_to_open);


    int count = 0;
    while (ros::ok())
    {

        std_msgs::String msg;

        std::stringstream ss;
        ss << "hello world " << count;
        msg.data = ss.str();

        ROS_INFO("%s", msg.data.c_str());

        chatter_pub.publish(msg);

        ros::spinOnce();

        loop_rate.sleep();
        ++count;

////

        readOnce ();

    }
//

    return 0;
}
