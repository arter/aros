/*
Copyright © 2021 Artem MELNYK.
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

union unsignedInt   {
  unsigned int in = 0;
  unsigned char out[2];
} 
uInt;


unsignedInt speedLeft;
unsignedInt speedRight;

void setup() {
  Serial.begin(115200);
  Serial.flush();
}


void loop(){

  delay(10);
  // fill the data
  speedLeft.in = 55255;
  speedRight.in = 22522;

  // send the data if ASCII symbol was sent
  if (Serial.available())
  {
    byte s = Serial.read();


    if (s == 'r')
    {
      asm volatile ("  jmp 0");
    }

    else if (s == 'e')//binary raw values
    {
      byte buff[] {speedLeft.out[0], speedLeft.out[1], speedRight.out[0], speedRight.out[1]};
      Serial.write(buff, sizeof(buff));
    }

    else if (s == 'd')//check the dataframe as ASCII
    {

      Serial.print (speedLeft.in);
      Serial.print (" ");
      Serial.print (speedRight.in);
      Serial.println();
    }

  }
}



